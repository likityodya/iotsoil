package com.codemobiles.cmiotkotlinfirebase;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0010\u001a\u00020\u0011J(\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u0013H\u0002J\u0012\u0010\u001a\u001a\u00020\u00132\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u000e\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u001fJ(\u0010 \u001a\u00020\u00132\u0006\u0010!\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020\u00182\u0006\u0010$\u001a\u00020\u0018H\u0002J\u001c\u0010%\u001a\u00020\u0018*\u00020\u00112\u0006\u0010&\u001a\u00020\u00182\b\b\u0002\u0010\'\u001a\u00020(R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/codemobiles/cmiotkotlinfirebase/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "databaseReference", "Lcom/google/firebase/database/DatabaseReference;", "firebaseDatabase", "Lcom/google/firebase/database/FirebaseDatabase;", "i", "", "mDistanceSeries", "Lcom/jjoe64/graphview/series/LineGraphSeries;", "Lcom/jjoe64/graphview/series/DataPoint;", "mHumidSeries", "mPhSeries", "mRef", "mTempSeries", "getCurrentDateTime", "Ljava/util/Date;", "getIntentView", "", "year", "monthOfYear", "dayOfMonth", "value", "", "listenerDataChange", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onReset", "v", "Landroid/view/View;", "writeNewPost", "light", "humidity", "ph", "temperature", "toString", "format", "locale", "Ljava/util/Locale;", "app_debug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity {
    private com.google.firebase.database.DatabaseReference mRef;
    private com.google.firebase.database.FirebaseDatabase firebaseDatabase;
    private com.google.firebase.database.DatabaseReference databaseReference;
    private com.jjoe64.graphview.series.LineGraphSeries<com.jjoe64.graphview.series.DataPoint> mTempSeries;
    private com.jjoe64.graphview.series.LineGraphSeries<com.jjoe64.graphview.series.DataPoint> mHumidSeries;
    private com.jjoe64.graphview.series.LineGraphSeries<com.jjoe64.graphview.series.DataPoint> mDistanceSeries;
    private com.jjoe64.graphview.series.LineGraphSeries<com.jjoe64.graphview.series.DataPoint> mPhSeries;
    private int i;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void getIntentView(int year, int monthOfYear, int dayOfMonth, java.lang.String value) {
    }
    
    public final void onReset(@org.jetbrains.annotations.NotNull()
    android.view.View v) {
    }
    
    private final void listenerDataChange() {
    }
    
    private final void writeNewPost(java.lang.String light, java.lang.String humidity, java.lang.String ph, java.lang.String temperature) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String toString(@org.jetbrains.annotations.NotNull()
    java.util.Date $this$toString, @org.jetbrains.annotations.NotNull()
    java.lang.String format, @org.jetbrains.annotations.NotNull()
    java.util.Locale locale) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Date getCurrentDateTime() {
        return null;
    }
    
    public MainActivity() {
        super();
    }
}
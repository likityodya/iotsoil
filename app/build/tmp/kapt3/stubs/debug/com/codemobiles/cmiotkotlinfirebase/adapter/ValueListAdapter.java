package com.codemobiles.cmiotkotlinfirebase.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B#\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0014H\u0016J\u0018\u0010\u0019\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0014H\u0016J\u0014\u0010\u001d\u001a\u00020\u00162\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00050\fR\u0016\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006\u001e"}, d2 = {"Lcom/codemobiles/cmiotkotlinfirebase/adapter/ValueListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/codemobiles/cmiotkotlinfirebase/adapter/OrderWaitingViewHolder;", "items", "Ljava/util/ArrayList;", "Lcom/codemobiles/cmiotkotlinfirebase/Model/ValueData;", "value_txt", "", "context", "Landroid/content/Context;", "(Ljava/util/ArrayList;Ljava/lang/String;Landroid/content/Context;)V", "clickListener", "Lcom/codemobiles/cmiotkotlinfirebase/adapter/ListClickListener;", "getContext", "()Landroid/content/Context;", "getItems", "()Ljava/util/ArrayList;", "getValue_txt", "()Ljava/lang/String;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setOnClickListener", "app_debug"})
public final class ValueListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.codemobiles.cmiotkotlinfirebase.adapter.OrderWaitingViewHolder> {
    private com.codemobiles.cmiotkotlinfirebase.adapter.ListClickListener<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> clickListener;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> items = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String value_txt = null;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.codemobiles.cmiotkotlinfirebase.adapter.OrderWaitingViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.codemobiles.cmiotkotlinfirebase.adapter.OrderWaitingViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void setOnClickListener(@org.jetbrains.annotations.NotNull()
    com.codemobiles.cmiotkotlinfirebase.adapter.ListClickListener<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> clickListener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> getItems() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getValue_txt() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public ValueListAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> items, @org.jetbrains.annotations.NotNull()
    java.lang.String value_txt, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}
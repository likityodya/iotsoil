package com.codemobiles.cmiotkotlinfirebase;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\u0012\u0010\u001a\u001a\u00020\u00192\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\b\u0010\u001d\u001a\u00020\u001eH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/codemobiles/cmiotkotlinfirebase/DataValueActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "setTAG", "(Ljava/lang/String;)V", "adapter", "Lcom/codemobiles/cmiotkotlinfirebase/adapter/ValueListAdapter;", "allTasks", "Ljava/util/ArrayList;", "Lcom/codemobiles/cmiotkotlinfirebase/Model/ValueData;", "getAllTasks", "()Ljava/util/ArrayList;", "setAllTasks", "(Ljava/util/ArrayList;)V", "date", "firebaseDatabase", "Lcom/google/firebase/database/FirebaseDatabase;", "value", "valueHisRef", "Lcom/google/firebase/database/DatabaseReference;", "init", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onSupportNavigateUp", "", "app_debug"})
public final class DataValueActivity extends androidx.appcompat.app.AppCompatActivity {
    private com.google.firebase.database.FirebaseDatabase firebaseDatabase;
    private com.google.firebase.database.DatabaseReference valueHisRef;
    private java.lang.String value;
    private java.lang.String date;
    private com.codemobiles.cmiotkotlinfirebase.adapter.ValueListAdapter adapter;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String TAG;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> allTasks;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    public final void setTAG(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> getAllTasks() {
        return null;
    }
    
    public final void setAllTasks(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.codemobiles.cmiotkotlinfirebase.Model.ValueData> p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void init() {
    }
    
    @java.lang.Override()
    public boolean onSupportNavigateUp() {
        return false;
    }
    
    public DataValueActivity() {
        super();
    }
}
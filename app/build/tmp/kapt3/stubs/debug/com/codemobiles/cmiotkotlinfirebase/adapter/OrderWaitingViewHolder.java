package com.codemobiles.cmiotkotlinfirebase.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u000e\u0010\r\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/codemobiles/cmiotkotlinfirebase/adapter/OrderWaitingViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "getView", "()Landroid/view/View;", "bindData", "", "data", "Lcom/codemobiles/cmiotkotlinfirebase/Model/ValueData;", "value_txt", "", "setOnClickListener", "onClick", "Landroid/view/View$OnClickListener;", "app_debug"})
public final class OrderWaitingViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
    @org.jetbrains.annotations.NotNull()
    private final android.view.View view = null;
    
    @android.annotation.SuppressLint(value = {"SetTextI18n"})
    public final void bindData(@org.jetbrains.annotations.NotNull()
    com.codemobiles.cmiotkotlinfirebase.Model.ValueData data, @org.jetbrains.annotations.NotNull()
    java.lang.String value_txt) {
    }
    
    public final void setOnClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View.OnClickListener onClick) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.view.View getView() {
        return null;
    }
    
    public OrderWaitingViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
        super(null);
    }
}
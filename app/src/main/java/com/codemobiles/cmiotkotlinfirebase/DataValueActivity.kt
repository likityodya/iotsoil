package com.codemobiles.cmiotkotlinfirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.codemobiles.cmiotkotlinfirebase.Model.ValueData
import com.codemobiles.cmiotkotlinfirebase.adapter.ValueListAdapter
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_data_value.*

class DataValueActivity : AppCompatActivity() {

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var valueHisRef: DatabaseReference
    private lateinit var value: String
    private lateinit var date: String
    private lateinit var adapter: ValueListAdapter

    var TAG = "Data:"
    var allTasks = ArrayList<ValueData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_value)

        init()
    }

    private fun init() {



        recycler_view.layoutManager = LinearLayoutManager(this)
        firebaseDatabase = FirebaseDatabase.getInstance()
        valueHisRef = firebaseDatabase!!.getReference()


        value = intent.getStringExtra("value").toString()
        date = intent.getStringExtra("date").toString()

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "$value $date"
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        valueHisRef = firebaseDatabase!!.getReference("history/$value/$date")


        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
//                val value = dataSnapshot.getValue(ValueData::class.java)
                // ...

                val children = dataSnapshot.children

                children.forEach {
                    allTasks.add(it.getValue(ValueData::class.java)!!)
                }

                adapter = ValueListAdapter(allTasks,value,this@DataValueActivity)
                recycler_view.adapter = adapter


            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        }
        valueHisRef.addListenerForSingleValueEvent(postListener)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

package com.codemobiles.cmiotkotlinfirebase

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import com.jjoe64.graphview.LegendRenderer
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.datatype.DatatypeConstants.MONTHS


class MainActivity : AppCompatActivity() {

    private lateinit var mRef: DatabaseReference
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference

    private var mTempSeries: LineGraphSeries<DataPoint>? = null
    private var mHumidSeries: LineGraphSeries<DataPoint>? = null
    private var mDistanceSeries: LineGraphSeries<DataPoint>? = null
    private var mPhSeries: LineGraphSeries<DataPoint>? = null
    private var i = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Listen to Data Change in Firebased DB
        listenerDataChange()

        // Setup Graph
        mTempSeries = LineGraphSeries()
        mHumidSeries = LineGraphSeries()
        mDistanceSeries = LineGraphSeries()
        mPhSeries = LineGraphSeries()

        mGraphView!!.addSeries(mTempSeries!!)
        mGraphView!!.addSeries(mHumidSeries!!)
        mGraphView!!.addSeries(mDistanceSeries!!)
        mGraphView!!.addSeries(mPhSeries!!)

        mGraphView!!.title = "Temp,Humid,Light,Ph. / Time"
        mTempSeries!!.title = "Temp"
        mHumidSeries!!.title = "Humid"
        mDistanceSeries!!.title = "Distance"
        mPhSeries!!.title = "Ph"

        mHumidSeries!!.color = Color.RED
        mDistanceSeries!!.color = Color.GREEN
        mPhSeries!!.color = Color.BLACK

        mTempSeries!!.isDrawDataPoints = true
        mTempSeries!!.dataPointsRadius = 10f
        mHumidSeries!!.isDrawDataPoints = true
        mHumidSeries!!.dataPointsRadius = 10f
        mDistanceSeries!!.isDrawDataPoints = true
        mDistanceSeries!!.dataPointsRadius = 10f
        mPhSeries!!.isDrawDataPoints = true
        mPhSeries!!.dataPointsRadius = 10f

        mGraphView!!.legendRenderer.isVisible = true
        mGraphView!!.legendRenderer.align = LegendRenderer.LegendAlign.TOP
        mGraphView!!.legendRenderer.textColor = Color.parseColor("#FFFFFF")

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        mInfraredTxt.setOnClickListener {

            val dpd = DatePickerDialog(this
                , DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date

                    getIntentView(year, monthOfYear+1, dayOfMonth,"light")

                }, year, month, day)

            dpd.show()
        }

        mTempTxt.setOnClickListener{
            val dpd = DatePickerDialog(this
                , DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date

                    getIntentView(year, monthOfYear+1, dayOfMonth,"temperature")

                }, year, month, day)

            dpd.show()
        }

        mHumidTxt.setOnClickListener{
            val dpd = DatePickerDialog(this
                , DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date

                    getIntentView(year, monthOfYear+1, dayOfMonth,"humidity")

                }, year, month, day)

            dpd.show()
        }

        mPhTxt.setOnClickListener{
            val dpd = DatePickerDialog(this
                , DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date

                    getIntentView(year, monthOfYear+1, dayOfMonth,"ph")

                }, year, month, day)

            dpd.show()
        }
    }

    private fun getIntentView(year: Int, monthOfYear: Int, dayOfMonth: Int,value: String) {
        val date = "" + year + "-" + monthOfYear + "-" + dayOfMonth
        val intent = Intent(this, DataValueActivity::class.java)
        intent.putExtra("date", date)
        intent.putExtra("value", value)
        startActivity(intent)
    }

    public fun onReset(v: View) {
        // Update IRSensorReset in Firebase DB to reset counter in Arduino Board
        val resetStatus = HashMap<String, Any>()
        resetStatus["IRSensorReset"] = true
        mRef.updateChildren(resetStatus)

        // Update IRSensorReset in Firebase DB to cancel resetting counter in Arduino Board
        Handler().postDelayed({
            resetStatus["IRSensorReset"] = false
            mRef.updateChildren(resetStatus)
        }, 1000)
    }

    private fun listenerDataChange() {
        mRef = FirebaseDatabase.getInstance().reference

        mRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                try {
                    // Get Values from Firebase DB
                    val map = dataSnapshot.value as Map<*, *>?
                    val ir = map!!["Light"].toString()
                    val temp = map["Temperature"].toString()
                    val humid = map["Humidity"].toString()
                    val distance = map["Light"].toString()
                    val ph = map["PH"].toString()


                    // Update UI
                    mInfraredTxt!!.text = "$ir lx"
                    mTempTxt!!.text = "$temp °C"
                    mHumidTxt!!.text = "$humid %"
                    mPhTxt !!.text = "$ph "


                    // Update DataPoint
                    mTempSeries!!.appendData(
                        DataPoint(
                            i.toDouble(),
                            Integer.parseInt(temp).toDouble()
                        ), false, 10
                    )
                    mHumidSeries!!.appendData(
                        DataPoint(
                            i.toDouble(),
                            Integer.parseInt(humid).toDouble()
                        ), false, 10
                    )
                    mDistanceSeries!!.appendData(
                        DataPoint(
                            i.toDouble(),
                            java.lang.Float.parseFloat(distance).toDouble()
                        ), false, 10
                    )
                    mPhSeries!!.appendData(
                        DataPoint(
                            i.toDouble(),
                            Integer.parseInt(ph).toDouble()
                        ), false, 10
                    )
                    i++

                } catch (e: Exception) {
                    Toast.makeText(this@MainActivity, e.message, Toast.LENGTH_SHORT).show()
                }

            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.getReference()

        databaseReference.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                Log.e("main","onChildAdded")

            }
            override fun onChildChanged(dataSnapshot: DataSnapshot, s: String?) {
                Log.e("main","onChildChanged")
                try {
                    // Get Values from Firebase DB
//                    val map = dataSnapshot.value as Map<*, *>?
                    val key = dataSnapshot.key.toString()
                    val value = dataSnapshot.value.toString()
                    var ir = ""
                    var temp = ""
                    var humid = ""
                    var distance = ""
                    var ph = ""

                    if (key.equals("Light")){
                        ir = value
                    }
                    if (key.equals("Temperature")) {
                        temp = value
                    }
                    if (key.equals("Humidity")) {
                        humid = value
                    }
                    if (key.equals("Light")) {
                        distance = value
                    }
                    if (key.equals("PH")) {
                        ph = value
                    }

                writeNewPost(ir,humid,ph,temp)


                } catch (e: Exception) {
//                    Toast.makeText(this@MainActivity, e.message, Toast.LENGTH_SHORT).show()
                }
            }
            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                Log.e("main","onChildRemoved")
            }
            override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {
                Log.e("main","onChildMoved")
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("main","onCancelled")
            }
        })

    }


    private fun writeNewPost(light: String, humidity: String, ph: String, temperature: String) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously

        val date = getCurrentDateTime()
        val dateInString = date.toString("yyyy-MM-dd")
//        val dateInString = "2019-11-21"

        if (light != "") {
            val key_light = mRef.child("history").child("light").child(dateInString).push().key
            if (key_light == null) {
                return
            }
            val fromDb: Map<String, Any> = mapOf(
                "value" to light
            )

            val childUpdates = HashMap<String, Any>()
            childUpdates["/history/light/$dateInString/$key_light"] = fromDb
            mRef.updateChildren(childUpdates)
        }

        if (humidity != "") {
            val key_humidity = mRef.child("history").child("humidity").child(dateInString).push().key
            if (key_humidity == null) {
                return
            }
            val fromDb: Map<String, Any> = mapOf(
                "value" to humidity
            )

            val childUpdates = HashMap<String, Any>()
            childUpdates["/history/humidity/$dateInString/$key_humidity"] = fromDb
            mRef.updateChildren(childUpdates)
        }

        if (ph != "") {
            val key_ph = mRef.child("history").child("ph").child(dateInString).push().key
            if (key_ph == null) {
                return
            }
            val fromDb: Map<String, Any> = mapOf(
                "value" to ph
            )

            val childUpdates = HashMap<String, Any>()
            childUpdates["/history/ph/$dateInString/$key_ph"] = fromDb
            mRef.updateChildren(childUpdates)
        }

        if (temperature != "") {
            val key_temperature = mRef.child("history").child("temperature").child(dateInString).push().key
            if (key_temperature == null) {
                return
            }
            val fromDb: Map<String, Any> = mapOf(
                "value" to temperature
            )

            val childUpdates = HashMap<String, Any>()
            childUpdates["/history/temperature/$dateInString/$key_temperature"] = fromDb
            mRef.updateChildren(childUpdates)
        }
    }

    fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }

}

package com.codemobiles.cmiotkotlinfirebase.customview

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import android.graphics.Typeface
import com.codemobiles.cmiotkotlinfirebase.Contextor


class DigitalTextView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : TextView(context, attrs, defStyle, defStyleRes) {

    init {
        val customFont = Typeface.createFromAsset(Contextor.getInstance().context.assets, "fonts/Let's go Digital Regular.ttf")
        typeface = customFont
    }

}
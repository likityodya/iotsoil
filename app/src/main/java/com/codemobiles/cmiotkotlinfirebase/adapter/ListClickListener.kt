package com.codemobiles.cmiotkotlinfirebase.adapter

interface ListClickListener<T> {
     fun onClick(item: T)
}
package com.codemobiles.cmiotkotlinfirebase.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codemobiles.cmiotkotlinfirebase.Model.ValueData
import com.codemobiles.cmiotkotlinfirebase.R
import kotlinx.android.synthetic.main.recycler_value_item.view.*

import java.util.*


class ValueListAdapter(
    val items: ArrayList<ValueData>,
    val value_txt: String,
    val context: Context) : RecyclerView.Adapter<OrderWaitingViewHolder>() {

    private var clickListener: ListClickListener<ValueData>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  OrderWaitingViewHolder {
        return OrderWaitingViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_value_item, parent, false))
    }

    override fun onBindViewHolder(holder:  OrderWaitingViewHolder, position: Int) {
        val item = items[position]
        holder.bindData(item,value_txt)
        if (this.clickListener != null) {
            holder.setOnClickListener(View.OnClickListener {
                this.clickListener!!.onClick(item)
            })
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnClickListener(clickListener: ListClickListener<ValueData>) {
        this.clickListener = clickListener
    }

}

class OrderWaitingViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    @SuppressLint("SetTextI18n")
    fun bindData(data: ValueData,value_txt: String) {

        view.tv_name.setText(value_txt)
        view.tv_value.setText(data.Value)


    }

    fun setOnClickListener(onClick: View.OnClickListener) {
        this.view.list_item.setOnClickListener(onClick)
    }

}





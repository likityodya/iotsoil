package com.codemobiles.cmiotkotlinfirebase.Model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class ValueData(
    var Value: String? = ""
) {

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "Value" to Value
        )
    }
}